package org.mj.hz.config;

import cn.hutool.json.JSONConfig;
import lombok.Data;
import org.mj.hz.converter.json.MjFastjsonConverter;
import org.mj.hz.converter.json.MjJsonConverter;

@Data
public class MjJsonConfig {

    private Class<? extends MjJsonConverter> jsonClass;


    public MjJsonConfig() {
    }

    public MjJsonConfig(MjJsonConfigBuilder builder) {
        this.jsonClass =builder.getJsonClass();
    }
    @Data
    public static class MjJsonConfigBuilder{

        private Class<? extends MjJsonConverter> jsonClass;

        public  MjJsonConfigBuilder setJsonClass(Class<? extends MjJsonConverter> jsonClass){
            this.jsonClass = jsonClass;
            return this;
        }

        public MjJsonConfig build(){
            return new MjJsonConfig(this);
        }

    }
}
