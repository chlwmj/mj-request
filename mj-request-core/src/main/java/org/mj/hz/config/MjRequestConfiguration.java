package org.mj.hz.config;

import lombok.Data;
import org.mj.hz.converter.json.MjFastjsonConverter;
import org.mj.hz.converter.json.MjJsonConverter;
import org.mj.hz.converter.xml.MjDom4jConverter;
import org.mj.hz.converter.xml.MjXmlConverter;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Data
public class MjRequestConfiguration {

    /**
     * 超时时间
     */
    private Integer time;

    /**
     * 超时单位
     */
    private TimeUnit timeUnit;


    /**
     * JSON解析器
     */
    private MjJsonConverter mjJsonConverter;

    /**
     * xml解析器
     */
    private MjXmlConverter mjXmlConverter;


    public MjRequestConfiguration(){
        time = 3000;
        timeUnit = TimeUnit.MILLISECONDS;
        mjJsonConverter = new MjFastjsonConverter();
        mjXmlConverter = new MjDom4jConverter();
    }

    public MjRequestConfiguration(Integer time, TimeUnit timeUnit, MjJsonConverter mjJsonConverter, MjXmlConverter mjXmlConverter) {
        this.time = time;
        this.timeUnit = timeUnit;
        this.mjJsonConverter = mjJsonConverter;
        this.mjXmlConverter = mjXmlConverter;
    }
}
