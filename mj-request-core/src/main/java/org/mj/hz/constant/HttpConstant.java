package org.mj.hz.constant;

import cn.hutool.http.ContentType;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class HttpConstant {

    public static final Set<String> CONTENT_TYPES = new HashSet<>();
    static {
       CONTENT_TYPES.add( ContentType.TEXT_PLAIN.getValue());
       CONTENT_TYPES.add( ContentType.JSON.getValue());
       CONTENT_TYPES.add( ContentType.XML.getValue());
       CONTENT_TYPES.add( ContentType.MULTIPART.getValue());
       CONTENT_TYPES.add( ContentType.FORM_URLENCODED.getValue());
       CONTENT_TYPES.add( ContentType.OCTET_STREAM.getValue());
       CONTENT_TYPES.add( ContentType.TEXT_HTML.getValue());
       CONTENT_TYPES.add( ContentType.TEXT_XML.getValue());

    }
}
