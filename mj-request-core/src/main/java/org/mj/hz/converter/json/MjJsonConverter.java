package org.mj.hz.converter.json;

/**
 * JSON序列化接口
 * @param <T>
 */
public interface MjJsonConverter {

    <T> byte[] serialize(T t);

    <T>T deserialize(byte[] bytes,Class<T> targetType);
}
