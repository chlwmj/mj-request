package org.mj.hz.converter.json;

import org.mj.hz.config.MjJsonConfig;

import java.lang.reflect.InvocationTargetException;

public class MjJsonConverterFactory {


    public <T> T createMjJsonConverter(Class<T> jsonConverterClass ) {
        try {
            return  jsonConverterClass.getConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

}
