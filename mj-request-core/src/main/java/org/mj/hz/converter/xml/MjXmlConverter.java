package org.mj.hz.converter.xml;

/**
 * Xml解析器
 */
public interface MjXmlConverter {
    <T>T deserialize(byte[] bytes,Class<T> targetType);
}
