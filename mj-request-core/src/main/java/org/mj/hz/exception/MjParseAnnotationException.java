package org.mj.hz.exception;

import java.lang.annotation.Annotation;

public class MjParseAnnotationException extends RuntimeException{

    public MjParseAnnotationException(Annotation annotation) {
        this(String.format("%s 注解解析器不存在！",annotation.annotationType().getSimpleName()));
    }

    public MjParseAnnotationException(String message) {
        super(message);
    }
}
