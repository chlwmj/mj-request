package org.mj.hz.exception;

public class MjRuntimeException extends Exception {
    public MjRuntimeException(Throwable cause) {
        super(cause);
    }

    public MjRuntimeException(String message) {
        super(message);
    }
}
