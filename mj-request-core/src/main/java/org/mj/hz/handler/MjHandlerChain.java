package org.mj.hz.handler;

import org.mj.hz.handler.method.MjDeleteMethodHandler;
import org.mj.hz.handler.method.MjGetMethodHandler;
import org.mj.hz.handler.method.MjPostMethodHandler;
import org.mj.hz.handler.method.MjPutMethodHandler;
import org.mj.hz.handler.param.MjHeaderParamHandler;
import org.mj.hz.handler.param.MjJsonParamHandler;
import org.mj.hz.handler.param.MjParamParamHandler;
import org.mj.hz.http.BaseRequest;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

public class MjHandlerChain {
    /**
     * 请求方法处理器
     */
    private static List<MjMethodHandler> mjMethodHandlers;

    /**
     * 参数解析器
     */
    private static  List<MjParamHandler> mjParamHandlers;



    static {
        mjMethodHandlers = new ArrayList<>();
        mjMethodHandlers.add(new MjPostMethodHandler());
        mjMethodHandlers.add(new MjGetMethodHandler());
        mjMethodHandlers.add(new MjPutMethodHandler());
        mjMethodHandlers.add(new MjDeleteMethodHandler());

        mjParamHandlers = new ArrayList<>();
        mjParamHandlers.add(new MjHeaderParamHandler());
        mjParamHandlers.add(new MjParamParamHandler());
        mjParamHandlers.add(new MjJsonParamHandler());
    }
    /**
     * 参数处理器链
     * @param baseRequest
     * @param annotation
     * @param index
     */
    public static Boolean paramChain(BaseRequest baseRequest, Annotation annotation,Integer index) {
        for (MjParamHandler mjParamHandler : mjParamHandlers) {
            if (mjParamHandler.process(baseRequest, annotation, index)) {
                return true;
            }
        }
        return false;
    }


    /**
     * 方法处理器链
     * @param baseRequest
     * @param annotation
     */
    public static Boolean methodChain(BaseRequest baseRequest, Annotation annotation) {
        for (MjMethodHandler mjMethodHandler : mjMethodHandlers) {
            if (mjMethodHandler.process(baseRequest,annotation)) {
                return true;
            }
        }
        return false;
    }
}
