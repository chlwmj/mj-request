package org.mj.hz.handler;

import org.mj.hz.http.BaseRequest;

import java.lang.annotation.Annotation;

public abstract class MjParamHandler {

    /**
     * 注解
     */
    protected Annotation annotation;

    /**
     *  // 返回Boolean.TRUE = 成功
     *     // 返回Boolean.FALSE = 拒绝
     *     // 返回null = 交下一个处理
     * @param request
     * @return
     */
    public abstract Boolean process(BaseRequest request,Annotation annotation,Integer index);


    /**
     * 判断是否满足注解条件
     */
    protected abstract  Boolean isSatisfyCondition();



}
