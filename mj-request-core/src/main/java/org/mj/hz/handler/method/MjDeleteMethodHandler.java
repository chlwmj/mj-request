package org.mj.hz.handler.method;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import org.mj.hz.annotation.method.MjDelete;
import org.mj.hz.annotation.method.MjPut;
import org.mj.hz.handler.MjMethodHandler;
import org.mj.hz.http.BaseRequest;

import java.lang.annotation.Annotation;

public class MjDeleteMethodHandler extends MjMethodHandler {
    @Override
    public Boolean process(BaseRequest request, Annotation annotation) {
        super.annotation = annotation;
        if (isSatisfyCondition()) {
            MjDelete mjPut = (MjDelete) annotation;
            HttpRequest httpRequest = HttpRequest.delete(mjPut.baseUrl());
            request.setHttpRequest(httpRequest);
            return true;
        }
        return false;
    }

    @Override
    protected Boolean isSatisfyCondition() {
        return StrUtil.equals(MjDelete.class.getSimpleName(),annotation.annotationType().getSimpleName());
    }

}
