package org.mj.hz.handler.method;


import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.Method;
import org.mj.hz.annotation.method.MjGet;
import org.mj.hz.handler.MjMethodHandler;
import org.mj.hz.http.BaseRequest;

import java.lang.annotation.Annotation;

/**
 * @MjGet 解析器
 * @see org.mj.hz.annotation.method.MjGet
 */
public class MjGetMethodHandler extends MjMethodHandler {

    @Override
    public Boolean process(BaseRequest request, Annotation annotation) {
        super.annotation = annotation;
        if (isSatisfyCondition()) {
            MjGet mjGet = (MjGet) annotation;
            HttpRequest httpRequest = HttpRequest.get(mjGet.baseUrl());
            request.setHttpRequest(httpRequest);
            return true;
        }
        return false;
    }

    @Override
    public Boolean isSatisfyCondition() {
        return StrUtil.equals(MjGet.class.getSimpleName(), annotation.annotationType().getSimpleName());
    }

}
