package org.mj.hz.handler.method;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.ContentType;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.Method;
import org.mj.hz.annotation.method.MjPost;
import org.mj.hz.constant.HttpConstant;
import org.mj.hz.handler.MjMethodHandler;
import org.mj.hz.http.BaseRequest;

import java.lang.annotation.Annotation;
import java.util.Map;

/**
 * @MjPost 解析器
 * @see MjPost
 */
public class MjPostMethodHandler extends MjMethodHandler {


    @Override
    public Boolean process(BaseRequest request, Annotation annotation) {
        super.annotation = annotation;
        if (isSatisfyCondition()) {
            MjPost mjPost = (MjPost) annotation;
            HttpRequest httpRequest = HttpRequest.post(mjPost.baseUrl());
            request.setHttpRequest(httpRequest);
            return true;
        }
        return false;
    }

    @Override
    public Boolean isSatisfyCondition() {
        return  StrUtil.equals(MjPost.class.getSimpleName(),annotation.annotationType().getSimpleName());
    }


}
