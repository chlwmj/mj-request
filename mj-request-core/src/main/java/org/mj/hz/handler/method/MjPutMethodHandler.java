package org.mj.hz.handler.method;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import org.mj.hz.annotation.method.MjPut;
import org.mj.hz.handler.MjMethodHandler;
import org.mj.hz.http.BaseRequest;

import java.lang.annotation.Annotation;

/**
 * MjPut注解解析器
 * @see MjPut
 */
public class MjPutMethodHandler extends MjMethodHandler {
    @Override
    public Boolean process(BaseRequest request, Annotation annotation) {
        super.annotation = annotation;
        if (isSatisfyCondition()) {
            MjPut mjPut = (MjPut) annotation;
            HttpRequest httpRequest = HttpRequest.put(mjPut.baseUrl());
            request.setHttpRequest(httpRequest);
            return true;
        }
        return false;
    }

    @Override
    protected Boolean isSatisfyCondition() {
        return StrUtil.equals(MjPut.class.getSimpleName(),annotation.annotationType().getSimpleName());
    }
}
