package org.mj.hz.handler.param;

import cn.hutool.core.util.StrUtil;
import org.mj.hz.annotation.param.MjHeader;
import org.mj.hz.annotation.param.MjParam;
import org.mj.hz.handler.MjParamHandler;
import org.mj.hz.http.BaseRequest;

import java.lang.annotation.Annotation;
import java.util.Map;

/**
 * mjHeader处理器
 * @see MjHeader
 */
public class MjHeaderParamHandler extends MjParamHandler {
    @Override
    public Boolean process(BaseRequest request, Annotation annotation, Integer index) {
        super.annotation = annotation;
        if (isSatisfyCondition()) {//能解析
            Map<Integer, Object> headerMap = request.getMjPreRequest().getHeaderMap();
            MjHeader mjParams = (MjHeader) annotation;
            headerMap.put(index,mjParams.value());
            return true;
        }
        return false;
    }

    @Override
    public Boolean isSatisfyCondition() {
       return StrUtil.equals(MjHeader.class.getSimpleName(),annotation.annotationType().getSimpleName());
    }
}
