package org.mj.hz.handler.param;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.ContentType;
import org.mj.hz.annotation.param.MjJson;
import org.mj.hz.handler.MjParamHandler;
import org.mj.hz.http.BaseRequest;

import java.lang.annotation.Annotation;

/**
 * mjJson处理器
 * @see org.mj.hz.annotation.param.MjJson
 */
public class MjJsonParamHandler extends MjParamHandler {
    @Override
    public Boolean process(BaseRequest request, Annotation annotation, Integer index) {
        super.annotation = annotation;
        if (isSatisfyCondition()) {//能解析
            request.getHttpRequest().header("Content-Type",ContentType.JSON.getValue());
            request.setContentType(ContentType.JSON);
            return true;
        }
        return false;
    }

    @Override
    public Boolean isSatisfyCondition() {
        return StrUtil.equals(MjJson.class.getSimpleName(),annotation.annotationType().getSimpleName());
    }
}
