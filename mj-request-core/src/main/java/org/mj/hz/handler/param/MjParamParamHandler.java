package org.mj.hz.handler.param;

import cn.hutool.core.util.StrUtil;
import org.mj.hz.annotation.param.MjParam;
import org.mj.hz.handler.MjParamHandler;
import org.mj.hz.http.BaseRequest;

import java.lang.annotation.Annotation;
import java.util.Map;

/**
 * @MjParam处理器
 * @see MjParam
 */
public class MjParamParamHandler extends MjParamHandler {
    /**
     * 处理流程
     * @param request
     * @return
     */
    @Override
    public Boolean process(BaseRequest request,Annotation annotation,Integer index) {
        super.annotation = annotation;
        if (isSatisfyCondition()) {//能解析
            Map<Integer, Object> paramMap = request.getMjPreRequest().getParamMap();
            MjParam mjParams = (MjParam) annotation;
            paramMap.put(index,mjParams.value());
            return true;
        }
        return false;
    }

    @Override
    public Boolean isSatisfyCondition() {
        return StrUtil.equals(MjParam.class.getSimpleName(),annotation.annotationType().getSimpleName());
    }
}
