package org.mj.hz.http;

import cn.hutool.http.Header;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 前置请求
 */
@Data
public class MjPreRequest {
    /**
     * 参数解析map
     */
    private Map<Integer,Object> paramMap = new HashMap<>();

    /**
     * 请求头解析map
     */
    private Map<Integer,Object> headerMap = new HashMap<>();
}
