package org.mj.hz.proxy;

import java.lang.reflect.Proxy;

/**
 * 代理工厂
 * @param <T> 具体代理的类
 */
public class ProxyFactory <T>{

    private Class<T> interfaceClass;

    public ProxyFactory(Class<T> interfaceClass) {
        this.interfaceClass = interfaceClass;
    }

    public T create(){
        InterfaceProxyHandler<T> handler = new InterfaceProxyHandler<>(interfaceClass);
        return (T) Proxy.newProxyInstance(interfaceClass.getClassLoader(), new Class[]{interfaceClass}, handler);
    }
}
