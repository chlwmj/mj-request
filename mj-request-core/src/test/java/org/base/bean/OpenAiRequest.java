package org.base.bean;
/**
 * Copyright 2023 bejson.com
 */

import lombok.Data;

/**
 * Auto-generated: 2023-06-01 16:32:39
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class OpenAiRequest {

    private String prompt;
    private int n;
    private String size;
}