package org.base.bean;

import lombok.Data;

@Data
public class UserVo {
    private String name;
    private Integer age;
}
