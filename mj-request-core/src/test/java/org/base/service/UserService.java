package org.base.service;


import com.alibaba.fastjson.JSONObject;
import org.base.bean.UserVo;
import org.base.service.impl.UserServiceImpl;
import org.mj.hz.annotation.MjRollback;
import org.mj.hz.annotation.method.MjDelete;
import org.mj.hz.annotation.method.MjGet;
import org.mj.hz.annotation.param.MjHeader;
import org.mj.hz.annotation.param.MjJson;
import org.mj.hz.annotation.method.MjPost;
import org.mj.hz.annotation.param.MjParam;


@MjRollback(rollbackClass = UserServiceImpl.class)
public interface UserService {

    @MjDelete(baseUrl = "http://localhost:8849/testfeign")
    String getBaidu(@MjParam("search") String search ,@MjHeader("token") String token
            ,@MjHeader("userId") Integer userId,@MjHeader("Content-Type") String contentType);



    @MjPost(baseUrl = "http://localhost:8849/updateUserVo")
    void  updateUserVo(@MjJson UserVo userVo);


    @MjGet(baseUrl = "http://localhost:8849/getUserVo")
    UserVo getUserVo();


    @MjGet(baseUrl = "http://20.21.1.233:10001/wlsq/traffic/parkingLot/page?currentPage=1&pageSize=10")
    JSONObject getParkIot(@MjHeader("Authorization") String authorization,@MjHeader("Community-Id") Long communityId);
}
