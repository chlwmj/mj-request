package org.base.test;

import cn.hutool.http.ContentType;
import com.alibaba.fastjson.JSONObject;
import org.base.bean.UserVo;
import org.base.service.UserService;
import org.junit.Test;
import org.mj.hz.proxy.ProxyFactory;

import java.lang.module.Configuration;
import java.sql.SQLOutput;

public class ProxyFactoryTest {
    UserService userService = null;
    {
        ProxyFactory<UserService> userServiceProxyFactory = new ProxyFactory<>(UserService.class);
        userService = userServiceProxyFactory.create();
    }
//    @Test
//    public void getParamTest() {
//        String baidu = userService.getBaidu("this is demo"
//                ,"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9" +
//                ".eyJ1c2VySW5mbyI6eyJjb21tdW5pdHlNYXAiOnsiMTY0ODUwMDAzNDQ0ODIzNjU0NSI6IueHleeHlTA0MTnljYPljLrljYPpnaLmtYvor5Xlm57lvZIiLCIxNjM1NTIxNjQyNzg2NDc2MDM0Ijoi5pyq5p2lMSIsIjE2MjU2NjIwODkzODMyMzU1ODUiOiLmtYvor5XlupTnlKhsem0iLCIxNjIyNTA3Nzc3ODg2ODg3OTM3Ijoi5pyo5pyo5Y2D5Yy65Y2D6Z2i5rWL6K-VIiwiMTYxMzczODg2ODI4MzEwNTI4MSI6Iua1i-ivleekvuWMujMiLCIxNjEzNzI5NDU0NzU0OTc5ODQyIjoi5rWL6K-V56S-5Yy6MSIsIjE2MDc5MzI0ODQzODM3MzE3MTQiOiLmnKjmnKjnpL7ljLoxMjI4IiwiMTYwMjU3NDQ4ODAwMjYyNTUzOCI6IuacquadpeekvuWMui3mvJTnpLoiLCIxIjoi5pyq5p2l56S-5Yy6In0sImRlZmF1bHRDb21tdW5pdHlJZCI6MTYwMjU3NDQ4ODAwMjYyNTUzOCwibG9naW5OYW1lIjoiMTg3NjY2MzA4MTciLCJ0ZWxlcGhvbmUiOiIxODc2NjYzMDgxNyIsInVzZXJUeXBlIjoiMSIsInVzZXJJZCI6MTU1OTcxNzgyNDA3MzYwOTIxOCwidXNlcm5hbWUiOiLnpL7ljLrnrqHnkIblkZgif" +
//                "SwidXNlcl9uYW1lIjoi56S-5Yy6566h55CG5ZGYIiwic2NvcGUiOlsiYWxsIl0sImV4cCI6MTY4NTE3NjI3MywiYXV0aG9yaX" +
//                "RpZXMiOlsiMTYwMjU3NDQ4ODE0OTQyNjE3NyIsIjE1NTk3MjA1MDYyOTc0NjI3ODYiLCIxNjI1NjYyMDg5NTM0MjMwNTI5IiwiMTU1OTcyMjY3MzA0MTM1NDc1NCIsIjE2MDI1NzQ0OTcxMzM2MjUzNDciLCIxNjM1NTIxNjQyODQ5MzkwNTkzIiwiMTYwNzU5MTM4OTgyNTE0Mjc4NSIsIjE2MDc5MzM5ODk4MzY4Nzc4MjYiLCIxNjIwNjg3NjMwNzI1Mzg2MjQxIiwiMTYyMjc3MDY0ODk0MDAzMjAwMSIsIjE1OTA1MzAzNTE2OTkxODE1NzAiLCIxNjEzNzI5NDU0ODMwNDc3MzEzIiwiMTU2NDg4OTUyNDA3NjYyNTkyMiIsIjE2MDc5MzI0ODQ1MDExNzIyMjUiLCIxNjQ4NTAwMDM0NDk4NTY4MTk0IiwiMTYyMTQzNzg0NTM2MzI2NTUzNyIsIjE2MTM3Mzg4NjgzNzk1NzQyNzMiLCIxNjA3NTgyNTE3ODYxMzU1NTIxIl0sImp0aSI6IjhmZThkMjllLWIxZjYtNGMwNC04OTZmLTE0YTdhNTIyYjRkMiIsImNsaWVudF9pZCI6IldFQiJ9.Ggt7X6fwF8rz4AuJhaWEsy0PGPxeFpMfYocFRISZTZo",
//                1, ContentType.TEXT_PLAIN.getValue());
//        System.out.println(baidu);
//
//
//
//    }
//
//    @Test
//    public void  testPostJsonTest(){
//        UserVo userVoDemo  = new UserVo();
//        userVoDemo.setName("李四");
//        userVoDemo.setAge(19);
//        userService.updateUserVo(userVoDemo);
//    }
//
//    @Test
//    public void  testGetReturnJsonTest(){
//        UserVo userVo = userService.getUserVo();
//        System.out.println(userVo.toString());
//    }
//
//    @Test
//    public void testParkIot(){
//        JSONObject parkIot = userService.getParkIot("Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySW5mbyI6eyJjb21tdW5pdHlNYXAiOnsiMTY2Mjk5NjE1NzQ2ODYzNTEzOCI6IjXmnIgyOeWPt-ekvuWMuiJ9LCJkZWZhdWx0Q29tbXVuaXR5SWQiOjE2NjI5OTYxNTc0Njg2MzUxMzgsImxvZ2luTmFtZSI6IjE1MDE0MDQyMDU2IiwidGVsZXBob25lIjoiMTUwMTQwNDIwNTYiLCJ1c2VyVHlwZSI6IjEiLCJ1c2VySWQiOjE2NjI5OTYxNjcyMTYxOTc2MzMsInVzZXJuYW1lIjoiMjA1NuekvuWMuiJ9LCJ1c2VyX25hbWUiOiIyMDU256S-5Yy6Iiwic2NvcGUiOlsiYWxsIl0sImV4cCI6MTY4NTYwNTg2MiwiYXV0aG9yaXRpZXMiOlsiMTY2Mjk5NjE1NzU1NjcxNTUyMiJdLCJqdGkiOiJmZWQxMjg4ZS1jMWY2LTRhZDEtYjhkZS1lNjM2NGI1NTgyOWYiLCJjbGllbnRfaWQiOiJXRUIifQ.CSInK0n5orHNj8x3TR5JCXURVCQfxfy3lBMzkCOyvxw"
//                , 1662996157468635138L);
//        System.out.println(parkIot);
//    }
}
