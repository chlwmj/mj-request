package org.mj.hz;

import org.mj.hz.springboot.annotation.MjRequestScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MjRequestScan({"org.mj.hz.service"})
public class MjRequestExampleApplication {
    public static void main(String[] args) {
        SpringApplication.run(MjRequestExampleApplication.class);
    }
}
