package org.mj.hz.controller;

import cn.hutool.json.JSONObject;
import org.mj.hz.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MjRequestExampleController {

    @Autowired
    public UserService userService;


    @GetMapping("/")
    public void  get(){
        JSONObject aa = userService.getParkIot("aa", 20L);
        System.out.println(aa);
    }
}
