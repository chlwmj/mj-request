package org.mj.hz.service;

import cn.hutool.json.JSONObject;
import org.mj.hz.annotation.MjRollback;
import org.mj.hz.annotation.method.MjDelete;
import org.mj.hz.annotation.method.MjGet;
import org.mj.hz.annotation.param.MjHeader;
import org.mj.hz.annotation.param.MjParam;
import org.springframework.stereotype.Service;

@MjRollback
@Service
public interface UserService {

    @MjDelete(baseUrl = "http://localhost:8849/testfeign")
    String getBaidu(@MjParam("search") String search , @MjHeader("token") String token
            , @MjHeader("userId") Integer userId, @MjHeader("Content-Type") String contentType);


    @MjGet(baseUrl = "http://20.21.1.233:10001/wlsq/traffic/parkingLot/page?currentPage=1&pageSize=10")
    JSONObject getParkIot(@MjHeader("Authorization") String authorization, @MjHeader("Community-Id") Long communityId);
}
