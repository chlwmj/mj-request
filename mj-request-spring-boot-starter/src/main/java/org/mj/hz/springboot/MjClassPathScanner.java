package org.mj.hz.springboot;

import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;

import java.io.IOException;
import java.util.Set;

public class MjClassPathScanner extends ClassPathBeanDefinitionScanner {


    private final static String[] MJ_METHOD_ANNOTATION_NAMES = new String[] {
            "org.mj.hz.annotation.method.MjDelete",
            "org.mj.hz.annotation.method.MjGet",
            "org.mj.hz.annotation.method.MjPost",
            "org.mj.hz.annotation.method.MjPut"
    } ;

    private boolean allInterfaces = true;


    public MjClassPathScanner(BeanDefinitionRegistry registry) {
        super(registry, false);
        registerFilters();
    }

    private void registerFilters() {
        if (allInterfaces) {
            // include all interfaces
            addIncludeFilter((metadataReader, metadataReaderFactory) ->
                    interfaceFilter(metadataReader, metadataReaderFactory));
        }

        // exclude package-info.java
        addExcludeFilter((metadataReader, metadataReaderFactory) -> {
            String className = metadataReader.getClassMetadata().getClassName();
            return className.endsWith("package-info");
        });
    }



    @Override
    protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
        // 增加过滤，为接口类，并且接口上包含CkInterfaceAnnotation注解
        return beanDefinition.getMetadata().isInterface() &&
                beanDefinition.getMetadata().isIndependent();
    }

    @Override
    protected boolean checkCandidate(String beanName, BeanDefinition beanDefinition) {
        if (super.checkCandidate(beanName, beanDefinition)) {
            return true;
        } else {
            System.out.println("Skipping MapperFactoryBean with name '" + beanName + "' and '" + beanDefinition.getBeanClassName() + "' mapperInterface. Bean already defined with the same name!");
            return false;
        }
    }

    @Override
    public Set<BeanDefinitionHolder> doScan(String... basePackages) {
        // spring默认不会扫描接口，此处设置为true，不做过滤

        Set<BeanDefinitionHolder> beanDefinitions = super.doScan(basePackages);
        if (beanDefinitions.isEmpty()) {
            System.out.println("未扫描到有CkInterfaceAnnotation注解接口");
        } else {
            this.processBeanDefinitions(beanDefinitions);
        }

        return beanDefinitions;
    }

    private void processBeanDefinitions(Set<BeanDefinitionHolder> beanDefinitions) {
        beanDefinitions.forEach((BeanDefinitionHolder holder) -> {
            GenericBeanDefinition definition = (GenericBeanDefinition) holder.getBeanDefinition();
            String beanClassName = definition.getBeanClassName();
            System.out.println("接口名称" + beanClassName);
            definition.getConstructorArgumentValues().addGenericArgumentValue(beanClassName);
            definition.setBeanClass(MjRequestFactoryBean.class);
            definition.setAutowireMode(GenericBeanDefinition.AUTOWIRE_BY_TYPE);
        });
    }


    private boolean interfaceFilter(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory) {
        ClassMetadata classMetadata = metadataReader.getClassMetadata();
        if (!classMetadata.isInterface() || classMetadata.isFinal()) {
            return false;
        }

        String[] superClassNames = metadataReader.getClassMetadata().getInterfaceNames();
        boolean hasSuperForestClient = false;
        for (String superClassName : superClassNames) {
            try {
                MetadataReader superMetaReader = metadataReaderFactory.getMetadataReader(superClassName);
                hasSuperForestClient = interfaceFilter(superMetaReader, metadataReaderFactory);
            } catch (IOException e) {
            }
            if (hasSuperForestClient) {
                return true;
            }
        }

        AnnotationMetadata annotationMetadata = metadataReader.getAnnotationMetadata();
        for (String methodAnnName : MJ_METHOD_ANNOTATION_NAMES) {
            if (annotationMetadata.hasAnnotatedMethods(methodAnnName)) {
                return true;
            }
        }

/*
        Set<String> baseAnnNames = annotationMetadata.getAnnotationTypes();
        for (String annName : baseAnnNames) {
            try {
                Class<?> annType = Class.forName(annName);
                Annotation lcAnn = annType.getAnnotation(BaseLifeCycle.class);
                if (lcAnn != null) {
                    return true;
                }
                lcAnn = annType.getAnnotation(MethodLifeCycle.class);
                if (lcAnn != null) {
                    return true;
                }
            } catch (Throwable ignored) {
            }
        }
*/
        return false;
    }


}