package org.mj.hz.springboot;

import org.mj.hz.proxy.ProxyFactory;
import org.springframework.beans.factory.FactoryBean;

public class MjRequestFactoryBean<T> implements FactoryBean<T> {

    private Class<T> interfaceClass;

    public MjRequestFactoryBean(Class<T> interfaceClass) {
        this.interfaceClass = interfaceClass;
    }

    @Override
    public T getObject() throws Exception {
        return  new ProxyFactory<>(interfaceClass).create();
    }

    @Override
    public Class<?> getObjectType() {
        return interfaceClass;
    }
}
