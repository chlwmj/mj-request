package org.mj.hz.springboot.annotation;

import org.mj.hz.springboot.MjRequestRegister;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import({MjRequestRegister.class})
public @interface MjRequestScan {

    String[] basePackages() default {};


    String[] value();
}
