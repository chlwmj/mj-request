package org.mj.hz.springboot.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(MjRequestProperties.class)
public class MjRequestConfiguration {
}
