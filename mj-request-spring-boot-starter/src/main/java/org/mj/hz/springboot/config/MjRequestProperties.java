package org.mj.hz.springboot.config;


import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "mjrequest.core")
public class MjRequestProperties {

    /**
     * 超时时间
     */
    private Integer time;

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }
}
